import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class ImagePanel extends JPanel {

	private static final long serialVersionUID = 7228200509150858517L;
	private Image image;
	
	public ImagePanel(String img) {
		image = new ImageIcon(img).getImage();
		Dimension size = new Dimension(image.getWidth(null), image.getHeight(null));
	    setPreferredSize(size);
	    setMinimumSize(size);
	    setMaximumSize(size);
	    setSize(size);
	    setLayout(null);
	}
	
	public void setImage(String img) {
		image = new ImageIcon(img).getImage();
	}
	
	public void paintComponent(Graphics g) {
	    g.drawImage(image, 0, 0, null);
	}

}
