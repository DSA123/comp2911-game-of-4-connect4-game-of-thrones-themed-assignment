package engine;

public class HeroEngine extends NegamaxEngine {

	public HeroEngine() {
		super(8);
	}

	@Override
	public int evaluate(GameState state) {
		/*
		 * Produce an evaluation based on a linear combination of the difference
		 * in the number of winning lines and threat squares between the
		 * opponent and ourself.
		 * 
		 * A winning line is a line of the target (eg 4) number of consecutive 
		 * cells with at least one piece of the given player and none of its 
		 * opponent.
		 *
		 * A threat square is an empty square, which, if filled by this player,
		 * would give them victory. We discard any threats that can be 
		 * immediately parried as these will be picked up by the Negamax search.
		 * 
		 * This approach was suggested in the paper
		 * 	'Heuristics in the game of Connect-K', J Lam, UC Irvine
		 * but was implemented from scratch. 
		 */
		
		// Order of (four) directions; the reverse direction is the negation.
		int dr[] = {0, -1, -1, -1};
		int dc[] = {-1, -1, 0, 1};
		
		int deltaWinningLines = 0;
		int deltaThreatSquares = 0;
		
		for(int playerId=0;playerId<=1;playerId++) {
			int numWinningLines = 0;
			int numThreatSquares = 0;
			for(int i=0;i<state.getNumRows();i++) {
				for(int j=0;j<state.getNumCols();j++) {
					for(int k=0;k<4;k++) {
						int numMine = 0;
						boolean anyTheirs = false;
						boolean inBounds = true;
						for(int l=state.getGoal()-1;l>=0;l--) {
							int nr = i + dr[k]*l;
							int nc = j + dc[k]*l;
							if(nr >= 0 && nc >= 0 && 
								nr < state.getNumRows() &&
								nc < state.getNumCols()) {
								Integer cellOwner = state.getCellOwner(nr, nc);
								if(cellOwner != null) {
									if(cellOwner == playerId) {
										numMine++;
									} else {
										anyTheirs = true;
										break;
									}
								}
							} else {
								inBounds = false;
								break;
							}
						}
						
						if(numMine > 0 && !anyTheirs && inBounds) {
							numWinningLines++;
							if(numMine == state.getGoal()-1 && i > 0 && 
								state.getCellOwner(i-1, j) == null) {
								numThreatSquares++;
							}
						}
					}
				}
			}
			
			if(playerId == state.currentPlayer()) {
				deltaWinningLines += numWinningLines;
				deltaThreatSquares += numThreatSquares;
			} else {
				deltaWinningLines -= numWinningLines;
				deltaThreatSquares -= numThreatSquares;
			}
		}
		
		return 10*deltaThreatSquares + deltaWinningLines;
	}
}
