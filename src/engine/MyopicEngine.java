package engine;

/**
 * A Negamax engine that only looks 2 moves ahead, evaluating every position
 * as a draw except for terminal positions. As such, it blocks all immediate
 * threats, if possible to do so.
 */
public class MyopicEngine extends NegamaxEngine {
	public MyopicEngine() {
		super(2);
	}

	@Override
	public int evaluate(GameState state) {
		return 0;
	}
}
