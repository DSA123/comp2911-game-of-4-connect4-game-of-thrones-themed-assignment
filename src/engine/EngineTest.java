package engine;

import java.util.Scanner;

public class EngineTest {

	public EngineTest() {
		
	}
	
	public void run() {
		//Engine engine = new MyopicEngine();
		//Engine engine = new LookaheadEngine();
		Engine engine = new HeroEngine();
		playGame(engine);
	}
	
	private void playGame(Engine e) {
		System.out.printf("\n*** Starting new game!\n");
		Scanner sc = new Scanner(System.in);
		System.out.printf("Choose player id [0 or 1]: ");
		int userPlayer = sc.nextInt();
		System.out.printf("\n");
		
		e.newGame(6, 7, 1-userPlayer, 4);
		
		for(int i=0;i<42;i++) {
			if(i % 2 == userPlayer) {
				System.out.printf("Enter next move [col number 0-6]: ");
				int userMove = sc.nextInt();
				System.out.printf("\n");
				e.informMove(userMove);
			} else {
				int engineMove = e.makeMove();
				System.out.printf("Engine moves: %d\n", engineMove);
			}
		}
		sc.close();
	}

	public static void main(String[] args) {
		EngineTest tester = new EngineTest();
		tester.run();
	}

}
