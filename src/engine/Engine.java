package engine;

/**
 * An interface for an engine that plays a game of Connect-K on a grid with two 
 * players. It is simple and basic so as to allow the Engine to decide on its 
 * representation of the game and implementation of strategies.
 */
public interface Engine {
	/**
	 * Ask the engine to begin a new game with given board dimensions, 
	 * terminating any existing game.
	 * @param height Number of cells in the height of the grid.
	 * @param width Number of cells in the width of the grid.
	 * @param playerId Should be 0, if the engine moves first or 1 if the engine
	 * moves second.
	 * @param goal The number of consecutive filled cells required for a player
	 * to win (the value of K).
	 */
	public void newGame(int height, int width, int playerId, int goal);

	/**
	 * Inform the engine of the next move made. Must be a legal move (column
	 * index must be valid ie 0 <= column < width), column cannot already be
	 * full and it must be the opponent's turn to move. The game must still be
	 * in progress.
	 * @param column The zero-indexed column number of the next move.
	 */
	public void informMove(int column);
	
	/**
	 * Ask the engine to undo the last move (this could have been made by either
	 * player).
	 * @return true, if there is a move to be undone, or false otherwise.
	 */
	public boolean undo();

	/**
	 * Ask the engine to make its next move. Must be its turn to play and the
	 * game must still be in progress.
	 * @return The zero-indexed number of the column the engine would like to
	 * place its next piece. Guaranteed to be a legal move.
	 */
	public int makeMove();
}
