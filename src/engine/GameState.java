package engine;

import java.util.Stack;

class GameState implements Cloneable {
	// Dimensions of the grid.
	private int numRows;
	private int numCols;
	
	// Number of consecutive cells occupied in order to win. 
	private int numWin;
	
	// Move history.
	private Stack<Integer> moveHistory;
	
	// The current state of the board.
	// board[i][j] = the id (either 0 or 1) of the player owning the cell in row
	// column j, or null if neither player owns the square.
	private Integer[][] board;
	
	// Height of each column.
	private int[] columnHeights;
	
	/**
	 * Constructs a GameState object.
	 * @param height The height of the board.
	 * @param width The width of the board.
	 * @param goal The number of consecutive pieces required to win the game.
	 */
	GameState(int height, int width, int goal) {
		this.moveHistory = new Stack<Integer>();
		this.numRows = height;
		this.numCols = width;
		this.numWin = goal;
		this.board = new Integer[height][width];
		for(int i=0;i<this.numRows;i++) {
			for(int j=0;j<this.numCols;j++) {
				this.board[i][j] = null;
			}
		}
		this.columnHeights = new int[width];
		for(int i=0;i<this.numCols;i++) {
			columnHeights[i] = 0;
		}
	}
	
	/**
	 * Returns the number of rows in the board.
	 * @return Number of rows in the board.
	 */
	int getNumRows() {
		return numRows;
	}
	
	/**
	 * Returns the number of columns in the board.
	 * @return Number of columns in the board.
	 */
	int getNumCols() {
		return numCols;
	}
	
	/**
	 * Returns the number of consecutive cells required to win.
	 * @return Number of consecutive cells required to win.
	 */
	int getGoal() {
		return numWin;
	}
	
	/**
	 * Returns the owner of a valid given board cell.
	 * @param row The zero-indexed row of the cell.
	 * @param col The zero-indexed column of the cell. 
	 * @return The id of the player who owns the board cell, or null if no
	 * one does. 
	 */
	Integer getCellOwner(int row, int col) {
		return board[row][col];
	}
	
	/**
	 * Returns the id of the current player.
	 * @return The id of the current player.
	 */
	int currentPlayer() {
		return moveHistory.size() % 2;
	}
	
	/**
	 * Returns the id of the current player's opponent.
	 * @return The id of the current player's opponent.
	 */
	int otherPlayer() {
		return 1 - currentPlayer();
	}
	
	/**
	 * Attempts to make a move, altering the GameState.
	 * @param move The zero-indexed column to make the move in.
	 * @return true, if the move was valid, or false otherwise.
	 */
	boolean place(int move) {
		if(columnHeights[move] < numRows) {
			board[columnHeights[move]][move] = currentPlayer();
			columnHeights[move]++;
			moveHistory.add(move);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Attempts to make a move, altering the GameState.
	 * @return true, if there was a move to undo, or false otherwise.
	 */
	boolean undo() {
		if(moveHistory.isEmpty()) {
			return false;
		} else {
			int lastMove = moveHistory.pop();
			columnHeights[lastMove]--;
			board[columnHeights[lastMove]][lastMove] = null;
			return true;
		}
	}
	
	/**
	 * Checks if the board is full. Note this is different to whether or not
	 * the game is a draw.
	 * @return true, if the board is full, or false otherwise.
	 */
	boolean boardFull() {
		return moveHistory.size() == numRows * numCols;
	}
	
	/**
	 * Given the column the last move was made in, did the last move win the
	 * game?
	 * @param lastMove The column the last move was made in.
	 * @return true, if the last move was winning and false otherwise.
	 */
	boolean hasWon(int lastMove) {
		// Order of (four) directions; the reverse direction is the negation.
		int dr[] = {0, -1, -1, -1};
		int dc[] = {-1, -1, 0, 1};
		
		int lastPlayer = otherPlayer();
		int lastRow = columnHeights[lastMove]-1;
		int lastCol = lastMove;
		
		// Iterate through each pair and check.
		for(int z=0;z<4;z++) {
			int numHere = 0;
			// d is the directional sign.
			for(int d=-1;d<=1;d+=2) {
				int nr = lastRow;
				int nc = lastCol;
				for(int i=1;i<=3;i++) {
					nr += d*dr[z];
					nc += d*dc[z];
					if(nr >= 0 && nc >= 0 && nr < numRows && nc < numCols) {
						if(board[nr][nc] != null && board[nr][nc] == lastPlayer) {
							numHere++;
						} else {
							break;
						}
					} else {
						break;
					}
				}
			}
			
			if(numHere >= 3) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public GameState clone() {
		GameState g;
		try {
			g = (GameState)(super.clone());
			g.moveHistory = (Stack<Integer>)(this.moveHistory.clone());
			g.board = (Integer[][])(this.board.clone());
			g.columnHeights = (int[])(this.columnHeights.clone());
			return g;
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
}