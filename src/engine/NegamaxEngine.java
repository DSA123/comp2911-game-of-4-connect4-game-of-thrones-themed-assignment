package engine;

public abstract class NegamaxEngine implements Engine {
	// The value to assign a win.
	private final int WIN_EVAL = (1 << 30);
	
	// Maximum number of extra moves to look forward.
	private int maxDepth;
	
	// The actual state of the game.
	private GameState actualState;
		
	/**
	 * @param depth The maximum number of extra moves to look forward. Must be
	 * non-negative.
	 */
	public NegamaxEngine(int depth) {
		this.maxDepth = depth;
	}

	/**
	 * (Re)initialises the engine.
	 * @param height Number of rows in the grid.
	 * @param width Number of columns in the grid.
	 * @param playerId Which player I am.
	 * @param goal How many I need to get in a row to win.
	 */
	@Override
	public void newGame(int height, int width, int playerId, int goal) {
		this.actualState = new GameState(height, width, goal);
	}

	/**
	 * Handle a move by the current player.
	 * @param column The zero-indexed column of the last move.
	 */
	@Override
	public void informMove(int column) {
		actualState.place(column);
	}
	
	/**
	 * Handle a request to undo the last move.
	 * @return true, if there is a move to be undone, or false otherwise.
	 */
	public boolean undo() {
		return actualState.undo();
	}

	/**
	 * Determine the next move to make by using a Negamax search. Game must
	 * still be in progress.
	 * @return Column to play the next move in.
	 */
	@Override
	public int makeMove() {
		// Find best move and play it.
		GameState searchState = actualState.clone();
		MoveEval searchResult =
			search(searchState, maxDepth, -WIN_EVAL, WIN_EVAL);
		actualState.place(searchResult.getMove());
		//System.err.printf("heuristic=%d\n", searchResult.getEval());
		return searchResult.getMove();
	}
	
	/**
	 * Given a GameState, performs a Negamax search to find the optimal move to
	 * make, using Alpha-Beta pruning.
	 * @param state The current state of the search.
	 * @param remainingDepth The number of additional plies we are allowed to
	 * search through.
	 * @param alpha Pruning value for alpha; best known eval for current player.
	 * @param beta Pruning value for beta; best known eval for opponent.
	 * @return A MoveEval object explaining the best known move. Its move may
	 * be null if it is unsure how to proceed.
	 */
	private MoveEval search(GameState state, int remainingDepth, int alpha, 
		int beta) {
		// First, check for draw or if at max depth.
		if(state.boardFull()) {
			return new MoveEval(null, 0);
		} else if(remainingDepth == 0) {
			return new MoveEval(null, evaluate(state));
		} else {
			// Process each possible next move. Look at columns from the middle
			// out.
			MoveEval bestMove = null;
			int mid = state.getNumCols()/2;
			for(int it=0;it<state.getNumCols();it++) {
				// i is the column to move in.
				int i;
				if(it % 2 == 0) {
					i = mid - (it/2);
				} else {
					i = mid + ((it+1)/2);
				}
				if(state.place(i)) {
					//System.err.printf("ply=%d trying move %d alpha=%d beta=%d\n",remainingDepth,i,alpha,beta);
					// Check if won already.
					if(state.hasWon(i)) {
						//System.err.printf("WIN\n");
						state.undo();
						bestMove = new MoveEval(i, WIN_EVAL + remainingDepth);
						break;
					}
					MoveEval trueEval = new MoveEval(i, 
						-search(state, remainingDepth-1, -beta, -alpha).getEval());
					state.undo();
	
					// Update best, if applicable.
					if(bestMove == null || 
						bestMove.getEval() < trueEval.getEval()) {
						bestMove = trueEval;
					}
					
					// Update alpha and check prune condition.
					if(trueEval.getEval() > alpha) {
						alpha = trueEval.getEval();
					}
					
					if(alpha >= beta) {
						break;
					}
				}
			}
			return bestMove;
		}
	}
	
	/**
	 * @param state The current search state to evaluate.
	 * @return A numerical evaluation between -WIN_EVAL and WIN_EVAL inclusive,
	 * for the current player's position; a draw should be evaluated as 0.
	 */
	public abstract int evaluate(GameState state);
	
	/**
	 * A class storing a possible move on a board, and its evaluation, relative 
	 * to the current player.
	 */
	private class MoveEval implements Comparable<MoveEval> {
		private Integer move;
		private Integer eval;
		
		MoveEval(Integer move, Integer eval) {
			this.move = move;
			this.eval = eval;
		}
		
		int getMove() {
			return this.move;
		}
		
		int getEval() {
			return this.eval;
		}

		@Override
		public int compareTo(MoveEval o) {
			return o.eval - this.eval;
		}
	}
}