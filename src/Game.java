import java.util.LinkedList;

public class Game {

	private int[][] board;
	private int height;
	private int width;
	private int winLength;
	private boolean playerOne;
	private LinkedList<Integer> moveList;
	private LinkedList<Integer> redoList;

	/**
	 * Creates a new instance of connect <length> on a <height>x<width> board
	 * @param height the height of the board
	 * @param width the width of the board
	 * @param length the length to connect in order to win
	 */
	public Game(int height, int width, int length) {
		this.winLength = length;
		this.height = height;
		this.width = width;
		this.board = new int[width][height];
		this.playerOne = true;
		this.moveList = new LinkedList<Integer>();
		this.redoList = new LinkedList<Integer>(); 
	}

	/**
	 * Clears the current game board
	 */
	public void clearBoard() {
		for (int i = 0; i < this.height; i++) {
			for (int j = 0; j < this.width; j++) {
				this.board[i][j] = 0;
			}
		}
	}
	
	/**
	 * Alternates which player's turn it currently is
	 */
	public void alternatePlayer() {
		this.playerOne = !this.playerOne;
	}

	/**
	 * Gets the piece at the given row and column
	 * @param row the row
	 * @param column the column
	 * @return the piece at <row>, <column>
	 */
	public int getPiece(int row, int column) {
		return board[row][column];
	}
	
	/**
	 * Gets the height of the board
	 * @return height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Adds a piece to the given column if the column is not full yet
	 * @param column the column to place the piece
	 * @return true if column wasn't full, or false if column is full
	 */
	public boolean addPiece(int column) {
		int row = height-1;
		while (row >= 0) {
			if (this.board[column][row] == 0) {
				if (playerOne) {
					this.board[column][row] = 1;
				} else {
					this.board[column][row] = 2;
				}
				this.redoList.clear();
				this.moveList.add(column);
				this.moveList.add(row);
				//alternatePlayer();
				return true;
			}
			row--;
		}
		return false;
	}

	/**
	 * Returns if it's player one's turn or not
	 * @return
	 */
	public boolean playerOne() {
		return this.playerOne;
	}

	/**
	 * Undoes the most recently place piece. If no moves have been made in the game, it does nothing
	 */
	public void undo() {
		if (moveList.size() >= 2) {
			int row = this.moveList.removeLast();
			int column = this.moveList.removeLast();
			this.board[column][row] = 0;
			this.redoList.add(column);
			this.redoList.add(row);
			alternatePlayer();
		}
	}

	/**
	 * Redoes the last undone move. If no moves have been undone, it does nothing
	 */
	public void redo(){
		if (redoList.size() >= 2) {
			int row = this.redoList.removeLast();
			int column = this.redoList.removeLast();
			if (playerOne) {
				this.board[column][row] = 1;
			} else {
				this.board[column][row] = 2;
			}
			this.moveList.add(column);
			this.moveList.add(row);
			alternatePlayer();
		}
	}

	/**
	 * Checks if a win has ocurred yet
	 * @return 1 if player one has won, 2 if player two has won, -1 if it's a draw, or 0 otherwise
	 */
	public int checkWin(){
		// doesn't bother checking if no moves have been made
		if (this.moveList.size() > 2) {
			// extracts last move and restores moveList
			int row = this.moveList.removeLast();
			int col = this.moveList.removeLast();
			this.moveList.add(col);
			this.moveList.add(row);
			
			// checks if a winning move has been made
			if (checkHorizontal(row) || checkVertical(col)
				|| checkPosGradient(row-col) || checkNegGradient(row+col)) {
				if (playerOne()) {
					return 1;
				} else {
					return 2;
				}
			}

			// if there is no clear winner and board is full its a draw return -1
			if (this.moveList.size() == this.height * this.width * 2) {
				return -1;
			}	
		}

		// keep playing no winner return 0
		return 0;
	}

	/**
	 * Checks all potential horizontal wins for a row
	 * @param row the row to check
	 * @return if there was a win on this row
	 */
	private boolean checkHorizontal(int row) {
		int length = 1;
		for (int col = 0; col < this.width-1; col++) {
			if (this.board[col][row] == this.board[col+1][row] && this.board[col][row] != 0) {
				length++;
				if (length >= this.winLength) {
					return true;
				}
			} else {
				length = 1;
			}
		}
		return false;
	}
	
	/**
	 * Checks all potential vertical wins for a column
	 * @param col the column to check
	 * @return if there was a win on this column
	 */
	private boolean checkVertical(int col) {
		int length = 1;
		for (int row = 0; row < this.height-1; row++) {
			if (this.board[col][row] == this.board[col][row+1] && this.board[col][row] != 0) {
				length++;
				if (length >= this.winLength) {
					return true;
				}
			} else {
				length = 1;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param sum
	 * @return
	 */
	private boolean checkNegGradient(int sum) {
		int length = 1;
		for(int col = 0; col < sum; col++) {
			int row = sum-col;
			if(col < this.width-1 && row < this.height && row > 0) {
				if (this.board[col][row] == this.board[col+1][row-1] && this.board[col][row] != 0) {
					length++;
					if (length >= this.winLength) {
						return true;
					}
				} else {
					length = 1;
				}
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param row
	 * @return
	 */
	private boolean checkPosGradient(int row) {
		//sets row and col to the first diagonal
		int col = 0;
		int max = this.width;
		if (row < 0) {
			col = -row;
			row = 0;
			max = this.height;
		}

		int length = 1;

		//traverses down the diagonal checking if a player wins
		for (int i = 0; i < max; i++) {
			if (row+i < this.height-1 && col+i < this.width-1) {
				if (this.board[col+i][row+i] == this.board[col+i+1][row+i+1] && this.board[col+i][row+i] != 0) {
					length++;
					if (length >= this.winLength) {
						return true;
					}
				} else {
					length = 1;
				}
			}
		}
		return false;
	}

}
