import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;


public class GoTButton extends JButton {

	private static final long serialVersionUID = -837538747804381687L;
	private boolean background = true;

	public GoTButton(String text) {
		super(text);
		setup();
	}
	
	public GoTButton(String text, boolean background) {
		super(text);
		this.background = background;
		setup();
	}
	
	private void setup() {
		setFont(new Font("Arial", Font.BOLD, 20));
		if (background) {
			setBackground(new Color(50, 50, 50));
			setBorder(BorderFactory.createLineBorder(new Color(255, 255, 255), 2, false));
		} else {
			setOpaque(false);
			setContentAreaFilled(false);
			setBorder(BorderFactory.createEmptyBorder());
		}
		setForeground(new Color(255, 255, 255));
		setFocusPainted(false);
		addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				hoverOn();
			}
			
			public void mouseExited(MouseEvent e) {
				hoverOff();
			}
		});
	}
	
	private void hoverOn() {
		setBackground(new Color(60, 60, 60));
		setForeground(new Color(200, 50, 50));
	}
	
	private void hoverOff() {
		setBackground(new Color(50, 50, 50));
		setForeground(new Color(255, 255, 255));
	}
}
