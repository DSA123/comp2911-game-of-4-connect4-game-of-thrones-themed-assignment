/**
 * @author David Dimitriadis
 * This class is responsible for creating background music and sound effects.
 */
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Sound {

	private Clip clip;
	/**
	* Takes in a boolean and creates the necessary components to play the background music.
	* @precondition enableMusic == true | false
	* @param enableMusic The boolean used to decide whether to turn on the music or not (if its already on).
	* @exception  IOException If the music file cannot be found.
	* @exception LineUnavailableException If the data line cannot be opened.
	* @exception UnsupportedAudioFileException If the music file does not contain valid data of a recognized file type and format.
	*/
	public void playMusic(boolean enableMusic) {
		try {
			if (enableMusic) {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/GameOfThrones.wav").getAbsoluteFile());
			clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.loop(Clip.LOOP_CONTINUOUSLY);
			}
		} catch (Exception e) {}
	}
	
	/**
	* Creates the necessary components to play the click sound effect.
	*/
	public void playEffect() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/swordClash.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	
	public void dropPiece() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/throwKnife.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	
	public void winSound() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/celebration.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	/**
	* Stops the background music.
	*/
	public void stopMusic() {
		clip.stop();
	}
	
	public void loseEasy() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/AIeasyLose.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	
	public void loseMedium() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/AImedLose.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	
	public void loseHard() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/AIhardLose.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	
	public void winEasy() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/AIeasyWin.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	
	public void winMedium() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/AImedWin.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	
	public void winHard() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/AIhardWin.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	
	public void openEasy() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/AIeasyOpen.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	
	public void openMedium() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/AImedOpen.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	
	public void openHard() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/AIhardOpen.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	
	public void openTwoPlayer() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/TwoPlayerOpen.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	
	public void menuReturn() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/backToMenu.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	
	public void exitGame() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/Quit.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
	public void draw() {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("resources/audio/draw.wav").getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (Exception e) {}
	}
}
