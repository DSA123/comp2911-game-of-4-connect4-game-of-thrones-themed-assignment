/**
 * @author David Dimitriadis
 * This class is responsible for creating the MainMenu GUI, as well as the different aspects
 * of the GUI contained within it.
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import engine.Engine;
import engine.HeroEngine;
import engine.LookAheadEngine;
import engine.MyopicEngine;

public class MainMenu extends JFrame {
	
	private static final long serialVersionUID = 1068754898314749457L;
	private static Engine easyEngine = new MyopicEngine();
	private static Engine mediumEngine = new LookAheadEngine();
	private static Engine hardEngine = new HeroEngine();

	private boolean enableSoundEffects = true;
	private boolean enableMusic = true;
	private Sound music;
	//private JLabel panel;
	private JPanel panel;
	private ImageIcon background;
	private int currentEngine = 0;
	
	public void playSound() {
		if (enableSoundEffects) {
			Sound music = new Sound();
			music.playEffect();
		}
	}

	public MainMenu() {
		music = new Sound();
		music.playMusic(enableMusic);
		panel = new ImagePanel("resources/images/mainBg.jpg");
		panel.setBorder(BorderFactory.createEmptyBorder(20, 150, 20, 150));
		panel.setLayout(new GridLayout(7, 1, 4, 4));
		
		JButton easy = new GoTButton("Easy", false);
		JButton medium = new GoTButton("Medium", false);
		JButton hard = new GoTButton("Hard", false);
		JButton twoPlayers = new GoTButton("Two Players", false);
		JButton help = new GoTButton("Help", false);
		JButton options = new GoTButton("Options", false);
		JButton quit = new GoTButton("Quit", false);
		
		panel.add(easy);
		panel.add(medium);
		panel.add(hard);
		panel.add(twoPlayers);
		panel.add(options);
		panel.add(help);
		panel.add(quit);
		
		add(panel);
		
		final MainMenu self = this;
		easy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (enableSoundEffects == true) {
					Sound sound = new Sound();
					sound.openEasy();
				}
				GameScreen gameScreen = new GameScreen(self, easyEngine);
				currentEngine = 1;
				add(gameScreen);
				panel.setVisible(false);
				gameScreen.setVisible(true);
			}
		});
		
		medium.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (enableSoundEffects == true) {
					Sound sound = new Sound();
					sound.openMedium();
				}
				GameScreen gameScreen = new GameScreen(self, mediumEngine);
				currentEngine = 2;
				add(gameScreen);
				panel.setVisible(false);
				gameScreen.setVisible(true);
			}
		});
		
		hard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (enableSoundEffects == true) {
					Sound sound = new Sound();
					sound.openHard();
				}
				GameScreen gameScreen = new GameScreen(self, hardEngine);
				currentEngine = 3;
				add(gameScreen);
				panel.setVisible(false);
				gameScreen.setVisible(true);
			}
		});

		twoPlayers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (enableSoundEffects == true) {
					Sound sound = new Sound();
					sound.openTwoPlayer();
				}
				GameScreen gameScreen = new GameScreen(self);
				add(gameScreen);
				panel.setVisible(false);
				gameScreen.setVisible(true);
			}
		});
		
		options.addActionListener (new ActionListener() {
			public void actionPerformed (ActionEvent event) {
				playSound();
				JLabel soundEffects = new JLabel("Sound Effects");
				soundEffects.setFont(new Font("Arial", Font.BOLD, 20));
				soundEffects.setForeground(new Color(255, 255, 255));
				soundEffects.setHorizontalAlignment((int) CENTER_ALIGNMENT);
				JButton soundOn = new GoTButton("On");
				JButton soundOff = new GoTButton("Off");
				JLabel musicLabel = new JLabel("Music");
				musicLabel.setFont(new Font("Arial", Font.BOLD, 20));
				musicLabel.setForeground(new Color(255, 255, 255));
				musicLabel.setHorizontalAlignment((int) CENTER_ALIGNMENT);
				JButton musicOn = new GoTButton("On");
				JButton musicOff = new GoTButton("Off");
				JButton back = new GoTButton("Back to Menu");
				back.setHorizontalAlignment((int) CENTER_ALIGNMENT);
				final JPanel options = new ImagePanel("resources/images/mainBg.jpg");
				options.setLayout(new GridLayout(3, 3, 5, 5));
				options.setBorder(BorderFactory.createEmptyBorder(50,0,50,0));
				
				options.add(soundEffects);
				options.add(soundOn);
				options.add(soundOff);
				options.add(musicLabel);
				options.add(musicOn);
				options.add(musicOff);
				options.add(back);
				
				soundOn.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						enableSoundEffects = true;
						playSound();
					}
				});
				
				soundOff.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						enableSoundEffects = false;
					}
				});
				
				musicOn.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						playSound();
						if (!enableMusic) {
							music.playMusic(true);
						}
						enableMusic = true;
					}
				});
				
				musicOff.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						playSound();
						if (enableMusic) {
							music.stopMusic();
						}
						enableMusic = false;
					}
				});
				
				back.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent event) {
						options.setVisible(false);
						panel.setVisible(true);
						playSound();
					}
				});
				add(options);
				panel.setVisible(false);
				options.setVisible(true);
			}
		});
		
		help.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				playSound();
				final JPanel tutorial = new ImagePanel("resources/images/mainBg.jpg");
				tutorial.setLayout(new FlowLayout());
				tutorial.setBorder(BorderFactory.createEmptyBorder(20,0,20,0));
				try {
					BufferedImage rulesImage = ImageIO.read(new File("resources/images/Rules.png"));
					Image scaledImage = rulesImage.getScaledInstance(500, 325, Image.SCALE_SMOOTH);
					JLabel rulesLabel = new JLabel(new ImageIcon(scaledImage));
					rulesLabel.setHorizontalAlignment((int) CENTER_ALIGNMENT);
					tutorial.add(rulesLabel);
				} catch (IOException e) {}
				JButton back = new GoTButton("Back To Menu");
				back.setPreferredSize(new Dimension(300,100));
				back.setFont(new Font("Arial", Font.PLAIN, 20));
				tutorial.add(back);
				back.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent event) {
						tutorial.setVisible(false);
						panel.setVisible(true);
						playSound();
					}
				});
				add(tutorial);
				panel.setVisible(false);
				tutorial.setVisible(true);
			}
		});

		quit.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent event) {
		    	if (enableSoundEffects == true) {
		    		Sound sound = new Sound();
		    		sound.exitGame();
		    		try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {}
		    	}
		    	System.exit(0);
		    }
		});
		setTitle("Game of Fours");
		setSize(800, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
	}
	
	public Sound getMusic() {
		return this.music;
	}
	public void stopMusic(MainMenu gameMenu) {
		music.stopMusic();
		gameMenu.enableMusic = false;
	}
	public JPanel getPanel() {
		return this.panel;
	}
	
	public boolean getMusicStatus(){
		return this.enableMusic;
	}
	
	public void setMusicStatus(boolean enable) {
		this.enableMusic = enable;
	}
	
	public void setEffectStatus(boolean enable) {
		this.enableSoundEffects = enable;
	}
	
	public boolean getEffectStatus() {
		return this.enableSoundEffects;
	}
	
	public int getCurrentEngine() { 
		return this.currentEngine;
	}
	/**
	 * Creates a runnable version of the game which launches the MainMenu.
	 * @param args The String being passed as input //CHECK THIS WITH ROB
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				MainMenu game = new MainMenu();
			    game.setVisible(true);
			}
		});
	}
}
