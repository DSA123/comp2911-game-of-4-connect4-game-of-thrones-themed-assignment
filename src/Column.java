import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;

public class Column extends JPanel {

	private static final long serialVersionUID = 1068754898314749457L;

	private ArrayList<JLabel> rows;
	private ImageIcon player1;
	private ImageIcon player2;
	private ImageIcon noplayer;

	public Column(int height) {
		rows = new ArrayList<JLabel>();
		setOpaque(false);
		setBorder(BorderFactory.createEmptyBorder(0,10,0,10));
		setLayout(new GridLayout(7, 1, 7, 7));
		noplayer = new ImageIcon(getClass().getResource("resources/images/empty.png"));
		player1 = new ImageIcon(getClass().getResource("resources/images/player1.png"));
		player2 = new ImageIcon(getClass().getResource("resources/images/player2.png"));
		
		for (int i = 0; i < height; i++) {
			JLabel temp = new JLabel(noplayer);
			rows.add(temp);
			add(temp);
		}			
	}

	public void rowAddPiece(int i, int player) {
		if (player == 1) {
			rows.get(i).setIcon(player1);
		}
		if (player == 2) {
			rows.get(i).setIcon(player2);
		}
		if (player == 0) {
			rows.get(i).setIcon(noplayer);
		}
	}

	public JLabel getRow(int i) {
		return rows.get(i);
	}
}