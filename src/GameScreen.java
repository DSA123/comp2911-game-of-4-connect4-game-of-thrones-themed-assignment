import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import engine.Engine;

public class GameScreen extends ImagePanel {

	private static final long serialVersionUID = 1068754898314749457L;

	private int h = 6;
	private int w = 7;
	private JPanel gameSpace;
	private ImageIcon pin;
	private JLabel hand;
	private JPanel handPanel;
	private ArrayList<Column> columns;
	private JPanel cols;
	private JPanel buttons;
	private ImagePanel winScreen;
	private JLabel winMessage;
	private JButton newGame;
	private JButton quit;
	private JButton undo; 
	private JButton redo; 
	private JButton toggleSound; 
	private JButton backToMenu; 
	private Game board;
	private Engine engine = null;
	
	public GameScreen(final MainMenu mainMenu, Engine engine) {
		super("resources/images/mainBg.jpg");
		engine.newGame(h, w, 1, 4);
		this.engine = engine;
		render(mainMenu);
	}
	
	public GameScreen(final MainMenu mainMenu) {
		super("resources/images/mainBg.jpg");
		render(mainMenu);
	}

	public void render(final MainMenu mainMenu) {
        setLayout(new BorderLayout());
		board = new Game(h,w,4);
		gameSpace = new JPanel();
		gameSpace.setOpaque(false);
		gameSpace.setLayout(new BorderLayout());
		gameSpace.setBackground(new Color(150, 150, 220));
		pin = new ImageIcon(getClass().getResource("resources/images/pin.png"));
		hand = new JLabel(pin);
		handPanel = new JPanel();
		handPanel.setOpaque(false);
		handPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		winScreen = new ImagePanel("resources/images/lenWin.png");
		winScreen.setBorder(BorderFactory.createEmptyBorder(50, 200, 50, 200));
		winScreen.setLayout(new GridLayout(2, 1, 5, 5));
		winMessage = new JLabel();
		winMessage.setHorizontalAlignment((int) CENTER_ALIGNMENT);
		winMessage.setFont(new Font("Arial", Font.BOLD, 20));
		winMessage.setForeground(new Color(255, 255, 255));
		winScreen.add(winMessage);
		newGame = new GoTButton("New Game / Quit to Main Menu");
		newGame.setFont(new Font("Arial", Font.PLAIN, 18));
		winScreen.add(newGame);
	
		
		cols = new JPanel();
		cols.setOpaque(false);
		cols.setBorder(BorderFactory.createEmptyBorder(10, 30, 5, 30));
		cols.setLayout(new GridLayout(1, 7, 0, 0));
		
		columns = new ArrayList<Column>();
		for(int i = 0; i < w ; i++){
			Column temp = new Column(h);
			columns.add(temp);
			cols.add(temp);
		}

		
		gameSpace.add(cols);
		
		handPanel.add(hand);
		gameSpace.add(handPanel, BorderLayout.NORTH);
		
		buttons = new JPanel();
		buttons.setOpaque(false);
		buttons.setForeground(new Color(255, 255, 255));
		TitledBorder title = BorderFactory.createTitledBorder("Game Menu");
		title.setTitleFont(new Font("Arial", Font.PLAIN, 16));
        buttons.setBorder(title);
		buttons.setLayout(new GridLayout(5, 1, 5, 5));
		
		undo = new GoTButton("Undo");
		undo.setFont(new Font("Arial", Font.PLAIN, 18));
		redo = new GoTButton("Redo");
		redo.setFont(new Font("Arial", Font.PLAIN, 18));
		toggleSound = new GoTButton("Toggle Sound");
		toggleSound.setFont(new Font("Arial", Font.PLAIN, 18));
		backToMenu = new GoTButton("Main Menu");
		backToMenu.setFont(new Font("Arial", Font.PLAIN, 18));
		quit = new GoTButton("Rage Quit");
		quit.setFont(new Font("Arial", Font.PLAIN, 18));

		buttons.add(undo);
		buttons.add(redo);
		buttons.add(toggleSound);
		buttons.add(backToMenu);
		buttons.add(quit);

		add(gameSpace, BorderLayout.CENTER);
		add(buttons, BorderLayout.EAST);

		gameSpace.addMouseMotionListener(new MouseMotionListener(){
			public void mouseDragged(MouseEvent e) {}

			public void mouseMoved(MouseEvent e) {
				hand.setLocation(e.getX() - 35, 0);
				for (Column c : columns) {
					if (e.getX() > c.getX() && e.getX() < c.getX() + c.getWidth()) {
						c.setOpaque(true);
						c.setBackground(new Color(200, 0 ,0));
					} else {
						c.setOpaque(false);
						c.setBackground(null);
					}
				}
			}
		}); 
		
		gameSpace.addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent e) {
				boolean valid = false;
				int i = 0;
				for (Column c : columns) {
					if (e.getX() > c.getX() && e.getX() < c.getX() + c.getWidth()) {
						if (board.addPiece(i)) {
							valid = true;
						}
						Sound pieceEffect = new Sound();
						if (mainMenu.getEffectStatus() == true) {
							pieceEffect.dropPiece();
						}
						
						// Inform AI of move if engine was given.
						if (engine != null) {
							engine.informMove(i);
						}
						break;
					}
					i++;
				}
				
				if (valid) {
					int winStatus = checkForWin(mainMenu);
					updateBoard();
				
					if (winStatus == 0) {
						board.alternatePlayer();
					}

					// Make AI move if engine was given
					if (engine != null) {
						board.addPiece(engine.makeMove());
						checkForWin(mainMenu);
						updateBoard();
						if (winStatus == 0) {
							board.alternatePlayer();
						}
					}
				}
			}

			public void mousePressed(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}			
		});
		
		undo.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent event) {
		    	mainMenu.playSound();
		    	board.undo();
		    	if (engine != null) {
		    		board.undo();
		    	}
		    	updateBoard();
		    }
		});
		redo.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent event) {
		    	mainMenu.playSound();
		    	board.redo();
		    	if (engine != null) {
		    		board.redo();
		    	}
		    	updateBoard();
		    }
		});
		
		toggleSound.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent event) {
				Sound toggle = mainMenu.getMusic();
				if (mainMenu.getMusicStatus() == false) {
					mainMenu.setMusicStatus(true);
					toggle.playMusic(true);
				} else {
					mainMenu.setMusicStatus(false);
					toggle.stopMusic();
				}
				if (mainMenu.getEffectStatus() == false) {
					mainMenu.setEffectStatus(true);
				} else {
					mainMenu.setEffectStatus(false);
				}
				mainMenu.playSound();
			}
		});
		
		backToMenu.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent event) {
				if (mainMenu.getEffectStatus() == true) {
		    		Sound sound = new Sound();
		    		sound.menuReturn();
				}
				setVisible(false);
				mainMenu.getPanel().setVisible(true);
			}
		});

		newGame.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent event) {
				mainMenu.playSound();
				setVisible(false);
				mainMenu.getPanel().setVisible(true);
			}
		});

		quit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (mainMenu.getEffectStatus() == true) {
					Sound sound = new Sound();
					sound.exitGame();
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {}
				}
				System.exit(0);
			}
		});
	}
	
	private void updateBoard() {
		int j = 0;
		for (Column c : columns) {
			for (int i = 0; i < h; i++) {
				c.rowAddPiece(i, board.getPiece(j, i));
			}
			j++;
		}
	}
	
	private int checkForWin(MainMenu mainMenu) {
		int win = board.checkWin();
		if (win == 1) {
			gameSpace.setVisible(false);
			buttons.setVisible(false);
			add(winScreen, BorderLayout.CENTER);
			winMessage.setText("PLAYER ONE WINNER");
			if (mainMenu.getEffectStatus() == true) {
				Sound winSound = new Sound();
				if (mainMenu.getCurrentEngine() == 0) {
					//get win music for two player game
				} else if (mainMenu.getCurrentEngine() == 1) {
					winSound.winEasy();
				} else if (mainMenu.getCurrentEngine() == 2) {
					winSound.winMedium();
				} else if (mainMenu.getCurrentEngine() == 3) {
					winSound.winHard();
				}
			}
		} else if (win == 2) {
			gameSpace.setVisible(false);
			buttons.setVisible(false);
			winScreen.setImage("resources/images/tarWin.png");
			add(winScreen, BorderLayout.CENTER);
			winMessage.setText("PLAYER TWO WINNER");
			if (mainMenu.getEffectStatus() == true) {
				Sound winSound = new Sound();
				if (mainMenu.getCurrentEngine() == 0) {
					//get win music for two player game
				} else if (mainMenu.getCurrentEngine() == 1) {
					winSound.loseEasy();
				} else if (mainMenu.getCurrentEngine() == 2) {
					winSound.loseMedium();
				} else if (mainMenu.getCurrentEngine() == 3) {
					winSound.loseHard();
				}
			}
		} else if (win == -1) {
			gameSpace.setVisible(false);
			buttons.setVisible(false);
			winScreen.setImage("resources/images/draw.png");
			add(winScreen, BorderLayout.CENTER);
			winMessage.setText("DRAW");
			if (mainMenu.getEffectStatus() == true) {
				Sound winSound = new Sound();
				if (mainMenu.getCurrentEngine() == 0) {
					// get draw music for two player game
				} else {
					winSound.draw();
				} 
			}
		}
		return win;
	}

	public JButton getMenu() {
		return quit;
	}

	public JButton getNewGame() {
		return newGame;
	}

}
